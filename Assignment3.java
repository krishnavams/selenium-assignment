package selenium_l1;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assign_06 {
	private static WebDriver d1;
	@BeforeClass
	public static void setBaseURL()
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\RA20066292\\Downloads\\chromedriver_win32\\chromedriver.exe");
		d1=new ChromeDriver();
		d1.get("https://demo.opencart.com");

	}
	@Before
	public void login()
	{
		d1.findElement(By.xpath("//a[contains(@href,'https://demo.opencart.com/index.php?route=account/account')]")).click();
		d1.findElement(By.xpath("//a[contains(text(),'Login')]")).click();
		d1.findElement(By.xpath("//input[@id='input-email']")).sendKeys("reshmabobby.chintala@gmail.com");
		d1.findElement(By.xpath("//input[@id='input-password']")).sendKeys("Xyz@123");
		d1.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
		System.out.println("sucessfully logged in");

	}
	@Test
	public void test(){
		
		d1.findElement(By.xpath("//div[@id='search']/input")).sendKeys("iphone");
		d1.findElement(By.xpath("//div[@id='search']/span/button")).click();
		d1.findElement(By.xpath("//div[@id='content']/div[3]/div/div/div[1]/a/img")).click();
		d1.findElement(By.xpath("//input[@id='input-quantity']")).clear();
		d1.findElement(By.xpath("//input[@id='input-quantity']")).sendKeys("2");
		d1.findElement(By.xpath("//button[@id='button-cart']")).click();
		d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=checkout/cart']")).click();
	    String price=d1.findElement(By.xpath("//div[@id='content']/form/div/table/tbody/tr/td[6]")).getText().toString();
		System.out.println(price);
	    Double total_price=Double.parseDouble(price.substring(1));
		if(total_price<200.00)
		{
			d1.findElement(By.xpath("//div[@id='content']/form/div/table/tbody/tr/td[4]/div/span/button[2]")).click();
			d1.findElement(By.xpath("//div[@id='content']/div/div/a")).click();
			System.out.println("continue shopping");
		}
		
		
	}
	@After
	public void logout()
	{
		d1.findElement(By.xpath("//div[@id='top-links']/ul/li[2]/a/span[2]")).click();
		d1.findElement(By.xpath("//ul[@class='dropdown-menu dropdown-menu-right']/li[5]/a[@href='https://demo.opencart.com/index.php?route=account/logout']")).click();
		System.out.println("sucessfully logout");
	}
	@AfterClass
	public static void driver()
	{
		d1.close();
	}

}
