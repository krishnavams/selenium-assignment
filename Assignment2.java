package selenium_l1;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class Assign_04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
						// TODO Auto-generated method stub
				System.setProperty("webdriver.chrome.driver","C:\\Users\\RA20066292\\Downloads\\chromedriver_win32\\chromedriver.exe");
				WebDriver d1=new ChromeDriver();
				//home page launch
				d1.get("https://demo.opencart.com");
				//Login into democart
				d1.findElement(By.xpath("//a[contains(@href,'https://demo.opencart.com/index.php?route=account/account')]")).click();
				d1.findElement(By.xpath("//a[contains(text(),'Login')]")).click();
				
				d1.findElement(By.xpath("//input[@id='input-email']")).sendKeys("reshmabobby.chintala@gmail.com");
				d1.findElement(By.xpath("//input[@id='input-password']")).sendKeys("Xyz@123");
				d1.findElement(By.xpath("//input[@class='btn btn-primary']")).click();
				//searching 
				d1.findElement(By.xpath("//input[@class='form-control input-lg']")).sendKeys("iphone");
				d1.findElement(By.xpath("//button[@class='btn btn-default btn-lg']")).sendKeys(Keys.ENTER);
				//clicking on iphone and ipad tab
				Select se = new Select(d1.findElement(By.xpath("//select[@class='form-control']")));
				se.selectByValue("28");
				d1.findElement(By.xpath("//label[@class='checkbox-inline']")).click();
				d1.findElement(By.xpath("//input[@id='button-search']")).click();
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/category&path=24']")).click();
				//click on check list from high to low
				Select sel = new Select(d1.findElement(By.xpath("//select[@id='input-sort']")));
				sel.selectByValue("https://demo.opencart.com/index.php?route=product/category&path=24&sort=p.price&order=DESC");
				//clicking on first product and adding to compare product
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/product&path=24&product_id=29&sort=p.price&order=DESC']")).click();
				d1.findElement(By.xpath("//button[@data-original-title='Compare this Product']")).click();
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/category&path=24&sort=p.price&order=DESC']")).click();
				//clicking on second product and adding to compare product
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/product&path=24&product_id=40&sort=p.price&order=DESC']")).click();
				d1.findElement(By.xpath("//button[@data-original-title='Compare this Product']")).click();
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/category&path=24&sort=p.price&order=DESC']")).click();
				
				//clicking on third product and adding to compare product
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/product&path=24&product_id=28&sort=p.price&order=DESC']")).click();
				d1.findElement(By.xpath("//button[@data-original-title='Compare this Product']")).click();
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/category&path=24&sort=p.price&order=DESC']")).click();
				d1.findElement(By.xpath("//a[@id='compare-total']")).click();
				//getting fifth feature from table and saving it into a file
				
				String info = d1.findElement(By.xpath("//*[@id=\"content\"]/table/tbody[1]/tr[5]/td[2]")).getText();

				String idForTxtFile = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());

				File file = new File("Phone_data" + idForTxtFile);



				try {

				  FileWriter fw = new FileWriter(file, true);

				  String lineSeparator = System.getProperty("line.separator");

				  String[] output = info.split("\n");

				  for (int i = 0; i <= output.length-1; i++) {

				    fw.write(output[i]);

				    fw.write(lineSeparator);

				  }





				  fw.write(info);



				  fw.flush();

				  fw.close();

				} catch (IOException e) {

				  System.out.println(e);
			}
				//clicking on first phone in the table and adding to cart
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=product/product&product_id=29']")).click();
				d1.findElement(By.xpath("//button[@id='button-cart']")).click();
				//click on check out 
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=checkout/cart']")).click();
				d1.findElement(By.xpath("//a[@href='https://demo.opencart.com/index.php?route=checkout/checkout']")).click();
				//clicking on order history from myaccount tab
				d1.findElement(By.xpath("//a[contains(@href,'https://demo.opencart.com/index.php?route=account/account')]")).click();
				d1.findElement(By.xpath("//div[@id='top-links']/ul/li[2]/ul/li[2]/a")).click();
				d1.findElement(By.xpath("//aside[@id='column-right']/div/a[12]")).click();
				//Newsletter subscribtion
				d1.findElement(By.xpath("//div[@id='content']/form/fieldset/div/div/label[1]/input")).click();
				d1.findElement(By.xpath("//div[@id='content']/form/div/div[2]/input")).click();
				//extra special in the footer
				d1.findElement(By.xpath("//div[@class='container']/div/div[3]/ul/li[4]/a")).click();
				
				d1.findElement(By.xpath("//button[@id='list-view']")).click();
				//logout
				d1.findElement(By.xpath("//a[contains(@href,'https://demo.opencart.com/index.php?route=account/account')]")).click();
				d1.findElement(By.xpath("//div[@id='top-links']/ul/li[2]/ul/li[5]/a")).click();
				d1.close();
			}
		

	}

